import sys
import redis


def get_args():
    args = sys.argv

    host = filter(lambda a: 'REDIS_HOST' in a, args)
    port = filter(lambda a: 'REDIS_PORT' in a, args)

    artifact_prefix = filter(lambda a: 'ARTIFACT_PREFIX' in a, args)
    app_version = filter(lambda a: 'VERSION' in a, args)
    licenser_token = filter(lambda a: 'LICENSER_ACCESS_TOKEN' in a, args)

    redis_args = list(host) + list(port)
    pipeline_args = list(artifact_prefix) + list(app_version) + list(licenser_token)

    return redis_args + pipeline_args


arg_list = get_args()

def get_redis():
    host = arg_list[0]
    port = arg_list[1]

    return redis.StrictRedis(host[11::], int(port[11::]))


r = get_redis()


def get_variables_block(key):
    keys = r.hgetall(key)

    result = ''
    for k, v in keys.items():
        k = k.decode('utf-8')
        v = v.decode('utf-8')
        if k == 'LICENSER_URL':
            variable = f'    {k}: {v}?app_version={arg_list[3][8::]}\n'
        else:
            variable = f'    {k}: {v}\n'
        result += variable

    return result


def generate_pipeline():
    customers = r.keys("customer:*")

    result = """
stages:
    - prepare_staging

    """

    for c in customers:
        key = c.decode('utf-8')
        j = f"""
trigger-{key[9::]}-staging-job:
  stage: prepare_staging
  variables:
    CLIENT: {key[9::]}
    ARTIFACT_PREFIX: {arg_list[2][16::]}
    VERSION: {arg_list[3][8::]}
    LICENSER_ACCESS_TOKEN: {arg_list[4][22::]}
{get_variables_block(key)}
  trigger:
    include:
    - local: cicd/common-package-pipeline.yml
"""
        result += j

    print(result)

    f = open('child-pipeline-gitlab-ci.yml', "x")
    f.write(result)
    f.close()


if __name__ == '__main__':
    generate_pipeline()
    